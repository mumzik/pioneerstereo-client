﻿void prints(
	__global char* debugBuf,
			 int *debugOffset, 
	__constant char* str)
	{
		int offset=0;
		while(str[offset]!='\0'){
			debugBuf[(*debugOffset)++]=str[offset++];
		}
	}

	
void printi(
	__global char* debugBuf,
			 int* debugOffset,
			 int i)
	{
		char buf[6];
		if (i<0){
			i=-i;
			debugBuf[(*debugOffset)++]='-';
		}
		int offset=0;
		do {
			buf[offset++]=(i%10)+0x30;
			i/=10;
		} while(i>0);

		for (int i=0; i<offset; i++){
			debugBuf[(*debugOffset)++]=buf[offset-i-1];
		}
	}


void getRGB(
	int color,
	char *RGB)
	{
		RGB[0]	= (color & 0x00FF0000) >> 16;
		RGB[1] = (color & 0x0000FF00) >> 8;
		RGB[2] = (color & 0x000000FF);
	}
   
void getTileDisparity(
		__global char* debugBuf, 
				 int* debugOffset,
		__global const int* left, 
		__global const int* right,	
		__global const int* res,
				 int* bounds,
		__global const int* maxDisp,
		__global int* disparity,
			     int tileOffset)
	{
		
		const int w=bounds[2];
		const int h=bounds[3];
		
		char RGBL[3];
		char RGBR[3];
		int localOffset=tileOffset;
		int value=0;
		
		int x,y;
		int offsetR;
		int offsetL;
		int CR=res[0]-w;
		for (int d=1; d<=maxDisp[0]; d++){
			offsetR=bounds[1]*res[0]+bounds[0];
			offsetL=offsetR+d;
			for (y=0; y < h; y++){
				for(x = 0; x < w; x++){					
						getRGB(left[offsetL],RGBL);
						getRGB(right[offsetR],RGBR); /* TODO remove new row tail*/
						for (int i=0;i<3;i++){
							value+=abs(RGBL[i]-RGBR[i]);
						}
						offsetR++;
						offsetL++;
					}
					offsetR+=CR;
					offsetL+=CR;
				}			
			disparity[localOffset]=value;
			value=0;
			localOffset++;
		}
   }
   
__kernel void dispKernel (
		__global const int * left,
		__global const int * right,
		__global const int* res,
		__global const int* tilesCount,
		__global const int* maxDisp,
		__global int * result,
		__global char * debugBuf)
	{
		int tileX = get_global_id(0);
		int tileY = get_global_id(1);
		/*FOR DEBUG*/
		int debugOffset=(tileY*tilesCount[0]+tileX)*1024;
		
		int width=res[0]/tilesCount[0];
		int height=res[1]/tilesCount[1];
		/*
		DEBUG
		prints(debugBuf,&debugOffset,"tile x:");
		printi(debugBuf,&debugOffset,tx);
		prints(debugBuf,&debugOffset," y:");
		printi(debugBuf,&debugOffset,ty);
		prints(debugBuf,&debugOffset,"\n");*/
		int bounds[4];
		bounds[0]=tileX*width;
		bounds[1]=tileY*height;
		bounds[2]=width;
		bounds[3]=height;
		/*pixel start offset for tile*/
		int tileOffset=(tileY*tilesCount[0]+tileX)*(maxDisp[0]);
		getTileDisparity(debugBuf,&debugOffset,left,right,res,bounds,maxDisp,result,tileOffset);
	}  