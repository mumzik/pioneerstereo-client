﻿   
__kernel void depthKernel (
		__global const short * disparity,
		__global const double * Q,
		__global const int* res,
		__global const int* tileCnt,
		__global float * depth)
	{
		//service variables
		int tile = get_global_id(0);
		int width=res[0];
		int height=res[1];
		int dispLen=width*height;
		int startIndex=tile*dispLen/tileCnt[0];
		int endIndex=(tile+1)*dispLen/tileCnt[0];
		
		//used elements of Q 
		float cx=0;//(float) Q[3];
		float cy=0;//(float) Q[7];
		float f=(float) Q[11];
		float a43=(float) Q[14];
		float a44=(float) Q[15];
		
		//cycle variables
		int i3;
		float w;
		int xFlat;
		int yFlat;
		int wd2=width/2;
		int hd2=height/2;
		
		for (int i=startIndex; i<endIndex; i++){
			i3=i*3;
			xFlat=i%width-wd2;
			yFlat=i/width-hd2;
			w=a44+a43*disparity[i];
			depth[i3]=(cx+xFlat)/w;
			depth[i3+1]=(cy+yFlat)/w;
			depth[i3+2]=f/w; //z=f
		}
	}  