package ru.mumzik.robotics.exceptions;

import javafx.util.Pair;
import ru.mumzik.robotics.modules.ModuleConfiguration;
import ru.mumzik.robotics.modules.base.ModuleBase;

public class IncorrectConfigurationException extends Exception {
    private ModuleBase module;
    private Pair<String, String> parameter;

    public IncorrectConfigurationException(ModuleBase module, String key, String msg) {
        super(msg);
        this.module = module;
        this.parameter = new Pair<>(key, module.conf.getValue(key));
    }

    public Pair<String, String> getIncorrectParameter() {
        return parameter;
    }

    public ModuleBase getThrowsModule() {
        return module;
    }
}
