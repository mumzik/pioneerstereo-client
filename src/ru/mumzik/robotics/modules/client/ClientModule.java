package ru.mumzik.robotics.modules.client;

import ru.mumzik.net.MClient;
import ru.mumzik.net.TypedData;
import ru.mumzik.robotics.Terminal;
import ru.mumzik.robotics.exceptions.ModuleDetachedException;
import ru.mumzik.robotics.modules.base.ModuleBase;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.util.concurrent.TimeoutException;


//TODO errors check
public class ClientModule extends ModuleBase {
    private MClient client;
    private static ClientModule instance = new ClientModule();


    private ClientModule() {

    }

    /////////////
    //service API
    /////////////
    public static TypedData request(TypedData requestData) throws TimeoutException {
        try {
            return instance.client.request(requestData, instance.conf.getIntVal("requestTimeout"));
        } catch (InterruptedException ignored) {
        } catch (IOException e) {
            Terminal.criticalError("request IO exception");
        }
        return null;
    }

    ///////////////////////
    //module implementation
    ///////////////////////
    @Override
    protected File getModuleConfFile() {
        return new File(".\\conf\\client.cfg");
    }

    @Override
    protected void onStart() {
        client = new MClient();
        client.initDebug(0, System.out);
        client.unrecognizedIdListeners.add(() -> Terminal.error("unrecognized id response"));
        try {
            client.start(conf.getValue("serverIp"), conf.getIntVal("port"));
        } catch (ConnectException e) {
            Terminal.error(this, "server is not available");
        } catch (IOException e) {
            Terminal.error(this, "error occurred during client initialization");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onExit() {
        try {
            client.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public static ClientModule getInstance() {
        return instance;
    }

}
