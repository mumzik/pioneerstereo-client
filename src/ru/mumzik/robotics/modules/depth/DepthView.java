package ru.mumzik.robotics.modules.depth;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import ru.mumzik.opencv.util.MatUtil;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.base.View;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class DepthView extends View {
    private JLabel depthLabel =new JLabel();
    private long[] depthId=new long[]{0};

    public DepthView() {
        super("depth map", new ModuleBase[]{DepthModule.getInstance()});
        content.add(depthLabel);
        content.setLayout(new FlowLayout());
    }

    @Override
    protected void onUpdate() {
        Mat depth3 = DepthModule.depth.waitForNext(depthId);
        ArrayList<Mat> depths = new ArrayList<>();
        Core.split(depth3, depths);
        Mat depth8b = new Mat();
        Core.convertScaleAbs(depths.get(2), depth8b, 10000, 0);
        BufferedImage depth = MatUtil.matToImg(depth8b);
        depthLabel.setIcon(new ImageIcon(depth));
        depthLabel.setPreferredSize(new Dimension(depth.getWidth(), depth.getHeight()));
    }
}
