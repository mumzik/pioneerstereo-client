package ru.mumzik.robotics.modules.depth;

import org.opencv.core.Mat;
import ru.mumzik.robotics.Terminal;
import ru.mumzik.robotics.modules.DataService;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.disparity.DisparityModule;
import ru.mumzik.robotics.modules.rectification.StereoRectificationModule;
import ru.mumzik.robotics.util.ProcUtil;

import java.io.File;
import java.io.IOException;

public class DepthModule extends ModuleBase {
    public static final DataService<Mat> depth = new DataService<>();
    private static final DepthModule instance = new DepthModule();

    private DepthModule() {
        requiredModules.add(DisparityModule.getInstance());
        requiredModules.add(StereoRectificationModule.getInstance());
    }

    /////////////
    //service API
    /////////////

    ///////////////////////
    //module implementation
    ///////////////////////
    @Override
    protected File getModuleConfFile() {
        return new File(".\\conf\\depth.cfg");
    }

    @Override
    protected void onStart() {
        try {
            DepthEngine.init(".\\source\\kernels\\depth.cl");
        } catch (IOException e) {
            Terminal.criticalError("error reading kernel source");
        }
        long[] dispmapId = new long[]{0};
        addService(new Thread(() -> {
            ProcUtil.repeat(conf.getIntVal("updateInterval"), this::isAlive, () -> {
                Mat Q = StereoRectificationModule.getStereoCamera().getDispToDepthMat();
                Mat dispmap = DisparityModule.disparity.waitForNext(dispmapId);
                depth.set(DepthEngine.dispToDepth(dispmap, Q));

            });
        }));
    }

    @Override
    protected void onExit() {
        DepthEngine.dispose();
    }

    public static DepthModule getInstance() {
        return instance;
    }

    /////////////////
    //private section
    /////////////////
}
