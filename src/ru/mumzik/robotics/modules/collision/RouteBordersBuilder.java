package ru.mumzik.robotics.modules.collision;

import org.opencv.core.Mat;
import ru.mumzik.opencv.util.MatUtil;
import ru.mumzik.robotics.modules.DataService;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.rectification.StereoRectificationModule;
import ru.mumzik.robotics.modules.tracks.TracksBuilder;
import ru.mumzik.robotics.util.ProcUtil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

public class RouteBordersBuilder extends ModuleBase {
    private BufferedImage img;
    public static DataService<ArrayList<Section>> routeBorders=new DataService<>();
    public class Section {
        public double depth;
        public Point[] corners;
    }

    private static final RouteBordersBuilder instance = new RouteBordersBuilder();
   // private final DataService<ArrayList<Section>> routeBorders = new DataService<>();

    private RouteBordersBuilder() {
        requiredModules.add(TracksBuilder.getInstance());
        requiredModules.add(StereoRectificationModule.getInstance());
    }
    /////////////
    //Service API
    /////////////

    /**public static ArrayList<Section> getRouteBorders(){
        return instance.routeBorders.waitForData(instance::isAlive);
    }**/

    ///////////////////////
    //module implementation
    ///////////////////////
    @Override
    protected File getModuleConfFile() {
        return new File(".//conf//routeBorders.cfg");
    }

    @Override
    protected void onStart() {
        addService(new Thread(() -> {
            long[] trackId=new long[]{0};
            long[] stereoId=new long[]{0};
            double delta = 0.0005;//Double.parseDouble(conf.getValue("depthAccuracy"));
            double depthStep = 0.002;//Double.parseDouble(conf.getValue("depthStep"));
            ProcUtil.repeat(conf.getIntVal("updateInterval"), this::isAlive, () -> {
                ArrayList<Section> borders = new ArrayList<>();
                float[][] tracks = TracksBuilder.tracks.waitForNext(trackId);
                Mat[] stereoMat=StereoRectificationModule.rectifiedStereo.waitForNext(stereoId);
                img= MatUtil.matToImg(stereoMat[0]);
                int leftIndex = 0;
                int rightIndex = 0;
                double lastDepth = 0;
                while ((leftIndex*3 < tracks[0].length-3) && (rightIndex*3 < tracks[1].length-3)) {
                    Point[] sectionCorners = new Point[4];
                    double z1=tracks[0][leftIndex*3+2];
                    double z2=tracks[1][rightIndex*3+2];
                    if ((z1 - lastDepth) < depthStep) {
                        leftIndex++;
                        continue;
                    }
                    Point p1 = project(new float[]{tracks[0][leftIndex*3],tracks[0][leftIndex*3+1],tracks[0][leftIndex*3+2]});
                    Point p2 = project(new float[]{tracks[1][rightIndex*3],tracks[1][rightIndex*3+1],tracks[1][rightIndex*3+2]});

                    if (Math.abs(z2 - z1) < delta) {
                        sectionCorners[0] = p1;
                        sectionCorners[1] = p2;
                        sectionCorners[2] = new Point( p2.x - (p1.y - p2.y), p2.y + (p1.x - p2.x));
                        sectionCorners[3] = new Point( p1.x - (p1.y - p2.y), p1.y + (p1.x - p2.x));
                        Section section=new Section();
                        section.corners=sectionCorners;
                        section.depth=z1;
                        borders.add(section);
                        lastDepth = z1;
                        leftIndex++;
                        rightIndex++;
                    } else {
                        if (z1 > z2)
                            rightIndex++;
                        else
                            leftIndex++;
                    }
                }
                routeBorders.set(borders);
            });
        }));
    }

    @Override
    protected void onExit() {

    }

    private Point project(float[] point3) {
        Point point2=new Point();
        Mat QMat= StereoRectificationModule.getStereoCamera().getDispToDepthMat();
        double[] Q=new double[QMat.rows()*QMat.cols()];
        QMat.get(0,0,Q);
        point2.x=(int) (point3[0]*Q[11]/point3[2])+img.getWidth()/2;
        point2.y=(int) -(point3[1]*Q[11]/point3[2])+img.getHeight()/2;
        return point2;
    }

    public static RouteBordersBuilder getInstance() {
        return instance;
    }
}
