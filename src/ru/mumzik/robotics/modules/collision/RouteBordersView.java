package ru.mumzik.robotics.modules.collision;

import ru.mumzik.opencv.util.MatUtil;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.base.View;
import ru.mumzik.robotics.modules.rectification.StereoRectificationModule;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class RouteBordersView extends View {
    private BufferedImage img;
    private ArrayList<RouteBordersBuilder.Section> borders;
    private long[] stereoId=new long[]{0};
    private long[] boredrsId=new long[]{0};
    private JLabel borderLabel=new JLabel(){
        @Override
        public void paintComponent(Graphics g){
            super.paintComponent(g);
            g.setColor(Color.RED);
            g.drawImage(img,0,0,this);
            for (RouteBordersBuilder.Section s:borders){
                for (int i=0; i<4; i++){
                    g.drawLine(s.corners[i%4].x,s.corners[i%4].y,s.corners[(i+1)%4].x,s.corners[(i+1)%4].y);
                }
            }
        }
    };

    public RouteBordersView() {
        super("route borders", new ModuleBase[]{StereoRectificationModule.getInstance(),RouteBordersBuilder.getInstance()});
        content.setLayout(new FlowLayout());
        content.add(borderLabel);
    }

    @Override
    protected void onUpdate() {
        img= MatUtil.matToImg(StereoRectificationModule.rectifiedStereo.waitForNext(stereoId)[0]);
        borders=RouteBordersBuilder.routeBorders.waitForNext(boredrsId);
        borderLabel.setPreferredSize(new Dimension(img.getWidth(),img.getHeight()));
    }
}
