package ru.mumzik.robotics.modules.movements;


import ru.mumzik.gui.MControlButton;
import ru.mumzik.net.TypedData;
import ru.mumzik.robotics.Command;
import ru.mumzik.robotics.DataType;
import ru.mumzik.robotics.Terminal;
import ru.mumzik.robotics.modules.base.GuiModule;
import ru.mumzik.robotics.modules.base.ViewsModule;
import ru.mumzik.robotics.modules.base.TerminalModule;
import ru.mumzik.robotics.modules.client.ClientModule;
import ru.mumzik.robotics.modules.encoders.EncoderModule;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class MovementModule extends GuiModule {
    private static final int BUTTON_SIZE = 50;
    private static int VEL = 400;
    private static MovementModule instance=new MovementModule();

    private MovementModule(){
        requiredModules.add(ClientModule.getInstance());
    }

    ///////////////////////
    //module implementation
    ///////////////////////
    @Override
    protected File getModuleConfFile() {
        return new File(".\\conf\\movement.cfg");
    }

    @Override
    protected void onStart() {
        MControlButton f = new MControlButton(() -> power(VEL, -VEL), () -> power(0, 0));
        MControlButton b = new MControlButton(() -> power(-VEL, VEL), () -> power(0, 0));
        MControlButton l = new MControlButton(() -> power(-VEL, -VEL), () -> power(0, 0));
        MControlButton r = new MControlButton(() -> power(VEL, VEL), () -> power(0, 0));
        MControlButton s = new MControlButton(() -> power(0, 0), () -> power(0, 0));

        frame.add(f);
        frame.add(b);
        frame.add(l);
        frame.add(r);
        frame.add(s);


        frame.setSize(BUTTON_SIZE * 4, BUTTON_SIZE * 4);
        frame.setLayout(null);

        f.setBounds(BUTTON_SIZE, 0, BUTTON_SIZE, BUTTON_SIZE);
        b.setBounds(BUTTON_SIZE, BUTTON_SIZE * 2, BUTTON_SIZE, BUTTON_SIZE);
        l.setBounds(0, BUTTON_SIZE, BUTTON_SIZE, BUTTON_SIZE);
        r.setBounds(BUTTON_SIZE * 2, BUTTON_SIZE, BUTTON_SIZE, BUTTON_SIZE);
        s.setBounds(BUTTON_SIZE, BUTTON_SIZE, BUTTON_SIZE, BUTTON_SIZE);
    }

    @Override
    protected void onExit() {

    }


    public static TerminalModule getInstance() {
        return instance;
    }

    /////////////////
    //private section
    /////////////////
    private void power(int lVelocity, int rVelocity)  {
        try {
            TypedData requestData = new TypedData(DataType.TEXT, Command.left(lVelocity).getBytes(StandardCharsets.UTF_8));
            ClientModule.request(requestData);
            requestData = new TypedData(DataType.TEXT, Command.right(rVelocity).getBytes(StandardCharsets.UTF_8));
            ClientModule.request(requestData);
        }  catch (TimeoutException e) {
            Terminal.error(this, "request timeout");
        }
    }
}
