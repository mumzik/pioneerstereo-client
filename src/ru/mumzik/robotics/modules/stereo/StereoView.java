package ru.mumzik.robotics.modules.stereo;

import org.opencv.core.Mat;
import ru.mumzik.opencv.util.MatUtil;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.base.View;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class StereoView extends View {
    private JLabel[] stereoLabels = new JLabel[]{new JLabel(), new JLabel()};
    private long[] stereoId = new long[]{0};
    private BufferedImage[] stereoImg = new BufferedImage[2];

    public StereoView() {
        super("simple stereo", new ModuleBase[]{StereoModule.getInstance()});
        //init GUI
        for (int i = 0; i < 2; i++)
            content.add(stereoLabels[i]);
        content.setLayout(new FlowLayout());
    }

    @Override
    protected void onUpdate() {
        Mat[] stereo = StereoModule.rawSetereo.waitForNext(stereoId);
        for (int i = 0; i < 2; i++) {
            stereoImg[i] = MatUtil.matToImg(stereo[i]);
            stereoLabels[i].setIcon(new ImageIcon(stereoImg[i]));
            stereoLabels[i].setPreferredSize(new Dimension(stereoImg[i].getWidth(), stereoImg[i].getHeight()));
        }
    }
}
