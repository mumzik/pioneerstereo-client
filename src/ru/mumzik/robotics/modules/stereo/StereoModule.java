package ru.mumzik.robotics.modules.stereo;

import org.opencv.core.Mat;
import ru.mumzik.net.TypedData;
import ru.mumzik.opencv.util.MatUtil;
import ru.mumzik.robotics.Command;
import ru.mumzik.robotics.DataType;
import ru.mumzik.robotics.Terminal;
import ru.mumzik.robotics.modules.DataService;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.client.ClientModule;
import ru.mumzik.robotics.util.ImgBuilder;
import ru.mumzik.robotics.util.ProcUtil;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class StereoModule extends ModuleBase {
    public final static DataService<Mat[]> rawSetereo=new DataService<>();
    private final static StereoModule instance = new StereoModule();

    private StereoModule() {
        requiredModules.add(ClientModule.getInstance());
    }

    ///////////////////////
    //Module implementation
    ///////////////////////
    @Override
    protected void onStart() {
        addService(new Thread(() -> ProcUtil.repeat(conf.getIntVal("updateInterval"), this::isAlive, () -> {
            TypedData requestData = new TypedData(DataType.TEXT, Command.stereo().getBytes(StandardCharsets.UTF_8));
            TypedData responseData;
            try {
                responseData = ClientModule.request(requestData);
                if (responseData == null) {
                    return;
                }
                if (responseData.getType() == DataType.ERROR) {
                    Terminal.error(this, new String(requestData.getData(), StandardCharsets.UTF_8));
                }

                BufferedImage[] stereoImg = ImgBuilder.buildStereo(responseData.getData());
                Mat[] stereoBuf = new Mat[2];
                stereoBuf[0] = MatUtil.imgToMat(stereoImg[0]);
                stereoBuf[1] = MatUtil.imgToMat(stereoImg[1]);
                rawSetereo.set(stereoBuf);

            } catch (TimeoutException e) {
                Terminal.error(this, "request timeout");
            } catch (IOException e) {
                Terminal.error(this, "corrupt stereo data");
            }
        })));

    }


    @Override
    protected void onExit() {

    }

    @Override
    protected File getModuleConfFile() {
        return new File(".//conf//stereo.cfg");
    }

    public static StereoModule getInstance() {
        return instance;
    }


}
