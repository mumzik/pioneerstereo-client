package ru.mumzik.robotics.modules.disparity;

import org.opencv.calib3d.StereoSGBM;
import org.opencv.core.*;
import ru.mumzik.opencv.util.MatUtil;
import ru.mumzik.robotics.modules.DataService;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.rectification.StereoRectificationModule;
import ru.mumzik.robotics.util.ProcUtil;

import java.io.File;

public class DisparityModule extends ModuleBase {
    public static final DataService<Mat> disparity = new DataService<>();
    private static DisparityModule instance = new DisparityModule();

    private DisparityModule() {
        requiredModules.add(StereoRectificationModule.getInstance());
    }

    /////////////
    //service API
    /////////////

    ///////////////////////
    //module implementation
    ///////////////////////
    @Override
    protected File getModuleConfFile() {
        return new File(".\\conf\\disparity.cfg");
    }

    @Override
    protected void onStart() {
        StereoSGBM stereo = StereoSGBM.create();
        stereo.setPreFilterCap(conf.getIntVal("preFilterCap"));
        stereo.setUniquenessRatio(conf.getIntVal("uniquenessRatio"));
        stereo.setBlockSize(conf.getIntVal("blockSize"));
        stereo.setMinDisparity(conf.getIntVal("minDisparity"));
        stereo.setNumDisparities(conf.getIntVal("numDisparities"));
        stereo.setSpeckleRange(conf.getIntVal("speckleRange"));
        stereo.setSpeckleWindowSize(conf.getIntVal("windowSize"));

        long[] rectStereoId = new long[]{0};
        addService(new Thread(() -> ProcUtil.repeat(conf.getIntVal("updateInterval"), this::isAlive, () -> {
                Mat[] rectStereo = StereoRectificationModule.rectifiedStereo.waitForNext(rectStereoId);
                Mat disparityBuf = new Mat();
                stereo.compute(MatUtil.to8bit(rectStereo[0]), MatUtil.to8bit(rectStereo[1]), disparityBuf);
                disparity.set(disparityBuf);
        })));
    }

    @Override
    protected void onExit() {
    }

    public static DisparityModule getInstance() {
        return instance;
    }
}
