package ru.mumzik.robotics.modules.disparity;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import ru.mumzik.opencv.util.MatUtil;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.base.View;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class DisparityView extends View {
    private JLabel dispLabel =new JLabel();
    private long[] depthId=new long[]{0};


    public DisparityView() {
        super("disparity map", new ModuleBase[]{DisparityModule.getInstance()});
        content.add(dispLabel);
        content.setLayout(new FlowLayout());
    }

    @Override
    protected void onUpdate() {
        Mat dispNorm = new Mat();
        Mat disparity = DisparityModule.disparity.waitForNext(depthId);
        Core.normalize(disparity, dispNorm, 0, 255, Core.NORM_MINMAX, CvType.CV_8UC1);
        BufferedImage disparityImg = MatUtil.matToImg(dispNorm);
        dispLabel.setIcon(new ImageIcon(disparityImg));
        dispLabel.setPreferredSize(new Dimension(disparityImg.getWidth(), disparityImg.getHeight()));
    }
}
