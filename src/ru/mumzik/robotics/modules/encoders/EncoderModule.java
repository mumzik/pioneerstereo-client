package ru.mumzik.robotics.modules.encoders;

import org.opencv.core.Mat;
import ru.mumzik.net.TypedData;
import ru.mumzik.opencv.util.MatUtil;
import ru.mumzik.robotics.Command;
import ru.mumzik.robotics.DataType;
import ru.mumzik.robotics.Terminal;
import ru.mumzik.robotics.modules.DataService;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.client.ClientModule;
import ru.mumzik.robotics.util.ImgBuilder;
import ru.mumzik.robotics.util.ProcUtil;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

public class EncoderModule extends ModuleBase {
    private final static EncoderModule instance=new EncoderModule();
    public final static DataService<float[]> encoders=new DataService<>();


    private EncoderModule(){
        requiredModules.add(ClientModule.getInstance());
    }
    @Override
    protected File getModuleConfFile() {
        return new File(".//conf//encoders.cfg");
    }

    @Override
    protected void onStart() {
        addService(new Thread(() -> ProcUtil.repeat(conf.getIntVal("updateInterval"), this::isAlive, () -> {
            TypedData requestData = new TypedData(DataType.TEXT, Command.encoder().getBytes(StandardCharsets.UTF_8));
            TypedData responseData;
            float[] encodersBuf=new float[2];
            try {
                responseData = ClientModule.request(requestData);
                if (responseData == null) {
                    return;
                }
                if (responseData.getType() == DataType.ERROR) {
                    Terminal.error(this, new String(requestData.getData(), StandardCharsets.UTF_8));
                }
                for (int i=0; i<2; i++) {
                    byte[] byteBuf=Arrays.copyOfRange(responseData.getData(),i*4,(i+1)*4);
                    encodersBuf[i] = ByteBuffer.wrap(byteBuf).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                }
                encoders.set(encodersBuf);
            } catch (TimeoutException e) {
                Terminal.error(this, "request timeout");
            }
        })));
    }

    @Override
    protected void onExit() {

    }

    public static EncoderModule getInstance(){
        return instance;
    }
}
