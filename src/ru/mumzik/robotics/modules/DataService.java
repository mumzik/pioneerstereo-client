package ru.mumzik.robotics.modules;

public class DataService<T> {
    private long id;
    private T data;

    synchronized public void set(T data) {
        this.data = data;
        id = System.currentTimeMillis();
        this.notifyAll();
    }

    synchronized public T waitForNext(long[] id){
        if (this.id == id[0]) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        id[0]=this.id;
        return data;
    }

    synchronized public T get() {
        return data;
    }
}