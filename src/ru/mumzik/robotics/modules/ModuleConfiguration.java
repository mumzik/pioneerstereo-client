package ru.mumzik.robotics.modules;

import ru.mumzik.robotics.Terminal;
import ru.mumzik.robotics.modules.base.ModuleBase;

import java.io.*;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ModuleConfiguration {
    private static final Pattern VALID_CONF_STR = Pattern.compile("^\\s*(?<key>[^\\s:]+)\\s*:\\s*(?<value>[^\\s:]+)\\s*$");
    private static final Pattern EMPTY_CONF_STR = Pattern.compile("^[\\s\t]*$");

    private HashMap<String, String> data = new HashMap<>();
    private String filePath;

    public void load(File file) {
        filePath = file.getPath();
        if (!file.exists())
            Terminal.criticalError("configuration file \"" + filePath + "\" not found");
        else {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                String str;
                while ((str = br.readLine()) != null) {
                    parseStr(str);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseStr(String str) {
        Matcher m = VALID_CONF_STR.matcher(str);
        if (m.matches())
            data.put(m.group("key"), m.group("value"));
        else {
            m = EMPTY_CONF_STR.matcher(str);
            if (!m.matches())
                Terminal.criticalError("incorrect configuration format \"" + str + "\" in file:" + filePath);
        }
    }

    public String getValue(String key) {
        if (!data.containsKey(key)) {
            Terminal.criticalError("key \"" + key + "\" not found in file: " + filePath);
            return null;
        } else
            return data.get(key);
    }

    public int getIntVal(String key) {
        String value = getValue(key);
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            Terminal.criticalError("configuration value \"" + value + "\" in: " + filePath + " is not integer");
            return 0;
        }
    }
}
