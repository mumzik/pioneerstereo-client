package ru.mumzik.robotics.modules.tracks;

import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.util.ProcUtil;

import java.io.File;

public class TrackAnalyzer extends ModuleBase {

    private final static TrackAnalyzer instance = new TrackAnalyzer();
    //private final DataService<Boolean> freeDepth = new DataService<>();


    private TrackAnalyzer() {
        requiredModules.add(TracksBuilder.getInstance());
    }

    /////////////
    //service API
    /////////////

    ///////////////////////
    //Module implementation
    ///////////////////////
    @Override
    protected void onStart() {/**
        addService(new Thread(() -> {
            double maxWholeLength = Double.parseDouble(conf.getValue("maxWholeLength"));
            double maxObstacleHigh = Double.parseDouble(conf.getValue("maxObstacleHigh"));
            ProcUtil.repeat(conf.getIntVal("updateInterval"), this::isAlive, () -> {
                TracksBuilder.Track[] tracks = TracksBuilder.getTracks();
                boolean free = true;
                for (int i = 0; i < 2; i++) {
                    for (int d = 1; d < tracks[i].instance.size(); d++) {
                        //System.out.print(Math.abs(tracks[i].instance.get(d).coord.y) +" ");
                        if ((tracks[i].instance.get(d).coord.z - tracks[i].instance.get(d - 1).coord.z) > maxWholeLength) {
                            //System.out.println("whole");
                            free = false;
                        }
                        int d1 = d;
                        while ((d1 > 0) && ((tracks[i].instance.get(d).coord.z - tracks[i].instance.get(d1).coord.z) < maxWholeLength)) {
                            if (Math.abs(tracks[i].instance.get(d).coord.y - tracks[i].instance.get(d1).coord.y) > maxObstacleHigh) {
                                free = false;
                               // System.out.println("obstacle "+Math.abs(tracks[i].instance.get(d).y - tracks[i].instance.get(d1).y));
                            }
                            d1--;
                        }
                    }
                   // System.out.println();
                }
                freeDepth.setData(free);
            });
        }));**/

    }


    @Override
    protected void onExit() {
    }


    @Override
    protected File getModuleConfFile() {
        return new File(".//conf//tracksAnalyzer.cfg");
    }

    public static TrackAnalyzer getInstance() {
        return instance;
    }


}
