package ru.mumzik.robotics.modules.tracks;

import ru.mumzik.robotics.modules.DataService;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.util.ProcUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

public class AltitudeBuilder extends ModuleBase {
    public static final DataService<float[][]> altitudeFunction = new DataService<>();
    private final static AltitudeBuilder instance = new AltitudeBuilder();
    private final static float FILTER_INTERVAL = 0.0001f;
    private static final float TRACK_STEP = 0.0001f;
    private static final float TRACK_MAX_Y_DELTA = 0.05f;

    private AltitudeBuilder() {
        requiredModules.add(TracksBuilder.getInstance());
    }

    @Override
    protected File getModuleConfFile() {
        return new File(".\\conf\\altitudeBuilder.cfg");
    }

    @Override
    protected void onStart() {
        long[] trackId = new long[]{0};
        float[][] altitude = new float[2][];
        addService(new Thread(() -> ProcUtil.repeat(conf.getIntVal("updateInterval"), this::isAlive, () -> {
            ArrayList<LinkedList<float[]>> ways = new ArrayList<>();
            float[][] tracks = TracksBuilder.tracks.waitForNext(trackId);
            for (int t = 0; t < 2; t++) {

                float[] track = tracks[t];
                ArrayList<float[]> altitudeA=new ArrayList<>();
                LinkedList<float[]> interval = new LinkedList<>();
                //add first point
                interval.addFirst(new float[]{track[0], -track[1], track[2]});
                for (int i = 3; i < track.length-3; i += 3) {
                    //get xyz of current point
                    float x = track[i];
                    float y = -track[i + 1];
                    float z = track[i + 2];
                    if (y-interval.getLast()[1]> TRACK_MAX_Y_DELTA)
                        continue;
                    //check for depth step
                    if (Math.abs(z - interval.getFirst()[2]) > TRACK_STEP) {
                        float ys = 0;
                        float zs = 0;
                        //calc middle
                        for (float[] p : interval) {
                            ys += p[1];
                            zs += p[2];
                        }
                        int size = interval.size();
                       altitudeA.add(new float[]{zs / size,ys / size});
                        //new interval
                        interval.clear();
                    }
                    interval.addLast(new float[]{x, y, z});
                }
                altitude[t]=new float[altitudeA.size()*2];
                for (int i=0; i<altitudeA.size(); i++){
                    altitude[t][i*2]=altitudeA.get(i)[0];
                    altitude[t][i*2+1]=altitudeA.get(i)[1];
                }
            }

            /** for (int  i=0; i<track.length/3; i++){
             altitude[t][i*2]=track[i*3+2];
             altitude[t][i*2+1]=-track[i*3+1];
             }**/
            altitudeFunction.set(altitude);
        })));
    }

    @Override
    protected void onExit() {

    }

    public static AltitudeBuilder getInstance() {
        return instance;
    }
}
