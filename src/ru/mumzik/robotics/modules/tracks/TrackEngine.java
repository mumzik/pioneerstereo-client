package ru.mumzik.robotics.modules.tracks;

import javafx.util.Pair;
import org.jocl.*;
import org.opencv.core.Mat;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static org.jocl.CL.*;

public class TrackEngine {
    private static final int TILES = 600;
    private static final int TRACK_POINT_MAX_COUNT = 4096;
    private static cl_context context;
    private static cl_kernel kernel;
    private static cl_command_queue commandQueue;
    private static cl_program program;


    public static void init(String sourceFile) throws IOException {
        // The platform, device type and device number
        // that will be used
        final int platformIndex = 0;  //TODO
        final long deviceType = CL_DEVICE_TYPE_GPU;
        final int deviceIndex = 0;  //TODO

        // Enable exceptions and subsequently omit error checks in this sample
        CL.setExceptionsEnabled(true);

        // Obtain the number of platforms
        int numPlatformsArray[] = new int[1];
        clGetPlatformIDs(0, null, numPlatformsArray);
        int numPlatforms = numPlatformsArray[0];

        // Obtain a platform ID
        cl_platform_id platforms[] = new cl_platform_id[numPlatforms];
        clGetPlatformIDs(platforms.length, platforms, null);
        cl_platform_id platform = platforms[platformIndex];

        // Initialize the context properties
        cl_context_properties contextProperties = new cl_context_properties();
        contextProperties.addProperty(CL_CONTEXT_PLATFORM, platform);

        // Obtain the number of devices for the platform
        int numDevicesArray[] = new int[1];
        clGetDeviceIDs(platform, deviceType, 0, null, numDevicesArray);
        int numDevices = numDevicesArray[0];

        // Obtain a device ID
        cl_device_id devices[] = new cl_device_id[numDevices];
        clGetDeviceIDs(platform, deviceType, numDevices, devices, null);
        cl_device_id device = devices[deviceIndex];

        // Create a context for the selected device
        context = clCreateContext(
                contextProperties, 1, new cl_device_id[]{device},
                null, null, null);

        // Create a command-queue for the selected device
        commandQueue = clCreateCommandQueue(context, device, 0, null);


        // Create the program from the source code
        program = clCreateProgramWithSource(context,
                1, new String[]{getSource(sourceFile)}, null, null);

        // Build the program
        clBuildProgram(program, 0, null, null, null, null);

        // Create the kernel
        kernel = clCreateKernel(program, "tracksKernel", null);
    }

    public static Pair<float[],float[]> calcTrack(Mat depthMap, float[] parameters) {
        int w=depthMap.cols();
        int h=depthMap.rows();
        float[] depth=new float[w*h*depthMap.channels()];
        int[] res=new int[]{w,h};
        float[] track1=new float[TRACK_POINT_MAX_COUNT];
        float[] track2=new float[TRACK_POINT_MAX_COUNT];
        int[] tracksSize=new int[2];
        depthMap.get(0,0,depth);

        cl_mem memObjects[] = new cl_mem[7];
        memObjects[0] = clCreateBuffer(context,
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_float * depth.length , Pointer.to(depth), null);

        memObjects[1] = clCreateBuffer(context,
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_float*parameters.length, Pointer.to(parameters), null);
        memObjects[2] = clCreateBuffer(context,
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_int*res.length, Pointer.to(res), null);
        int[] tileCount=new int[]{TILES};
        memObjects[3] = clCreateBuffer(context,
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_int, Pointer.to(tileCount), null);
        memObjects[4] = clCreateBuffer(context,
                CL_MEM_READ_WRITE| CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_float*track1.length, Pointer.to(track1), null);
        memObjects[5] = clCreateBuffer(context,
                CL_MEM_READ_WRITE| CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_float*track2.length, Pointer.to(track2), null);
        memObjects[6] = clCreateBuffer(context,
                CL_MEM_READ_WRITE| CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_int*tracksSize.length, Pointer.to(tracksSize), null);

        // Set the arguments for the kernel
        for (int i = 0; i < memObjects.length; i++)
            clSetKernelArg(kernel, i,
                    Sizeof.cl_mem, Pointer.to(memObjects[i]));


        // Set the work-item dimensions
        //TODO tile count
        long global_work_size[] = new long[]{TILES};
        long local_work_size[] = new long[]{1};
        // Execute the kernel
        clEnqueueNDRangeKernel(commandQueue, kernel, 1, null,
                global_work_size, local_work_size, 0, null, null);
        // Read the output data
        clEnqueueReadBuffer(commandQueue, memObjects[4], CL_TRUE, 0,
                Sizeof.cl_float*track1.length, Pointer.to(track1), 0, null, null);
        clEnqueueReadBuffer(commandQueue, memObjects[5], CL_TRUE, 0,
                Sizeof.cl_float*track2.length, Pointer.to(track2), 0, null, null);
        clEnqueueReadBuffer(commandQueue, memObjects[6], CL_TRUE, 0,
                Sizeof.cl_int*tracksSize.length, Pointer.to(tracksSize), 0, null, null);
        // Release kernel, program, and memory objects
        for (cl_mem memObject : memObjects) clReleaseMemObject(memObject);
        //System.out.println("t1:"+tracksSize[0]+" t2:"+tracksSize[1]);

        float[] track1Final=new float[tracksSize[0]*3];
        float[] track2Final=new float[tracksSize[1]*3];
        //TODO kostyl
        int l1=track1.length>track1Final.length?track1Final.length:track1.length;
        int l2=track2.length>track2Final.length?track2Final.length:track2.length;
        System.arraycopy(track1,0,track1Final,0,l1);
        System.arraycopy(track2,0,track2Final,0,l2);
        return new Pair<>(track1Final,track2Final);
    }


    public static void dispose() {
        clReleaseKernel(kernel);
        clReleaseProgram(program);
        clReleaseCommandQueue(commandQueue);
        clReleaseContext(context);
    }

    private static String getSource(String sourceFile) throws IOException {
        String s;
        StringBuilder src = new StringBuilder();
        BufferedReader r = new BufferedReader(new FileReader(sourceFile));
        while ((s = r.readLine()) != null){
            src.append(s);
            src.append("\n");
        }
        return src.toString();
    }
}
