package ru.mumzik.robotics.modules.tracks;

import ru.mumzik.gui.MGraph;
import ru.mumzik.gui.layouts.VerticalLayout;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.base.View;

import javax.swing.*;
import java.awt.*;

public class AltitudesView extends View {
    private static final Color LEFT_ALTITUDE_COLOR = Color.RED;
    private static final Color RIGHT_ALTITUDE_COLOR = Color.BLUE;
    private static final Dimension DEFAULT_GRAPH_SIZE = new Dimension(300,200);
    private static final double[] GRAPH_Y_INTERVAL = new double[]{-1, 1};
    private float[][] altitudes;
    private long[] altitudeId=new long[]{0};
    private MGraph[] altitudesLabel = new MGraph[]{new MGraph(), new MGraph()};


    public AltitudesView() {
        super("altitudes", new ModuleBase[]{AltitudeBuilder.getInstance()});
        content.setLayout(new VerticalLayout(true));
        for (int t=0; t<2; t++){
            content.add(altitudesLabel[t]);
            altitudesLabel[t].setColor(t==0?LEFT_ALTITUDE_COLOR:RIGHT_ALTITUDE_COLOR);
            altitudesLabel[t].setYinterval(GRAPH_Y_INTERVAL[0],GRAPH_Y_INTERVAL[1]);
            altitudesLabel[t].setGraphSize(DEFAULT_GRAPH_SIZE);
        }
        content.doLayout();
    }

    @Override
    protected void onUpdate() {
        altitudes = AltitudeBuilder.altitudeFunction.waitForNext(altitudeId);
        for (int t = 0; t < 2; t++) {
            if (altitudes[t].length<2)
                continue;
            int pointCount = altitudes[t].length / 2;
            double[] x = new double[pointCount];
            double[] y = new double[pointCount];
            for (int i = 0; i < pointCount; i++) {
                x[i] = altitudes[t][i * 2];
                y[i] = altitudes[t][i * 2 + 1];
            }
            altitudesLabel[t].setData(x, y);
        }
    }
}
