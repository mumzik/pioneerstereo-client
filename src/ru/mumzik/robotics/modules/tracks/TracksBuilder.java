package ru.mumzik.robotics.modules.tracks;

import javafx.util.Pair;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point3;
import ru.mumzik.robotics.exceptions.IncorrectConfigurationException;
import ru.mumzik.robotics.modules.DataService;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.depth.DepthModule;
import ru.mumzik.robotics.util.ProcUtil;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class TracksBuilder extends ModuleBase {
    public final static DataService<float[][]> tracks = new DataService<>();
    private static final float MAX_TRACK_DEPTH = 0.01f;
    private static final float X_DELTA = 0.005f;
    private static final float MIN_TRACK_DEPTH = 0.002f;
    private static final float TRACK_STEP = 0.0005f;

    private static TracksBuilder instance = new TracksBuilder();

    private TracksBuilder() {
        requiredModules.add(DepthModule.getInstance());
    }

    /////////////
    //Service API
    /////////////

    ///////////////////////
    //module implementation
    ///////////////////////
    @Override
    protected File getModuleConfFile() {
        return new File(".\\conf\\tracksBuilder.cfg");
    }


    @Override
    protected void onStart() {
        addService(new Thread(() -> {
            try {
                TrackEngine.init(".\\source\\kernels\\tracks.cl");
            } catch (IOException e) {
                e.printStackTrace();
            }
            float tracksWidth = Float.parseFloat(conf.getValue("tracksWidth"));
            float[] parameters = new float[]{
                    MIN_TRACK_DEPTH,        //near border of track
                    MAX_TRACK_DEPTH,        //far border of track
                    -tracksWidth,           //x of left wheel
                    tracksWidth,            //x of right wheel
                    X_DELTA                 //max of (track x - wheel x)
            };
            long[] depthId = new long[]{0};
            ProcUtil.repeat(conf.getIntVal("updateInterval"), this::isAlive, () -> {
                //build track
                //DEBUG
                long startTime=System.currentTimeMillis();

                Mat depth = DepthModule.depth.waitForNext(depthId);
                //DEBUG
                long getDepthTime=System.currentTimeMillis();
               // if (getDepthTime-startTime>50)
                //    System.out.println("get depth time limit!("+(getDepthTime-startTime)+")");

                Pair<float[], float[]> rawTrackPair = TrackEngine.calcTrack(depth, parameters);
                float[][] rawTrack = new float[][]{rawTrackPair.getValue(), rawTrackPair.getKey()};
                //DEBUG
                long buildTime=System.currentTimeMillis();
                //if (buildTime-getDepthTime>30)
                //    System.out.println("build tracks time limit!("+(buildTime-getDepthTime)+")");

                //sort track
                sortTrack(rawTrack[0],0,rawTrack[0].length/3-1);
                sortTrack(rawTrack[1],0,rawTrack[1].length/3-1);
                //DEBUG
                long sortTime=System.currentTimeMillis();
                if (sortTime-buildTime>30)
                    System.out.println("sort tracks time limit!("+(sortTime-buildTime)+")");
                //filtering
                tracks.set(rawTrack);
            });
        }));
    }

    private void sortTrack(float[] track, int start, int end) {
            if (start >= end)
                return;
            int i = start, j = end;
            int cur = i - (i - j) / 2;
            while (i < j) {
                while (i < cur && (track[i*3+2] <= track[cur*3+2])) {
                    i++;
                }
                while (j > cur && (track[cur*3+2] <= track[j*3+2])) {
                    j--;
                }
                if (i < j) {
                    for (int k=0; k<3; k++) {
                        float temp = track[i*3+k];
                        track[i*3+k] = track[j*3+k];
                        track[j*3+k] = temp;
                    }
                    if (i == cur)
                        cur = j;
                    else if (j == cur)
                        cur = i;
                }
            }
            sortTrack(track,start, cur);
            sortTrack(track,cur+1, end);
    }


    @Override
    protected void onExit() {
    }

    public static TracksBuilder getInstance() {
        return instance;
    }

    /////////////////
    //private section
    /////////////////
    /**private Track[] calcTrack(Mat depth) {
     long time=System.currentTimeMillis();
     double tracksMaxWidth = Double.parseDouble(conf.getValue("tracksWidth"));
     double[] trackX = new double[]{-tracksMaxWidth, tracksMaxWidth};
     int h = depth.rows();
     int w = depth.cols();
     /**ArrayList<Mat> depths = new ArrayList<>();
     Core.split(depth, depths);
     float[][] d = new float[3][w * h];
     for (int i = 0; i < 3; i++)
     depths.get(i).get(0, 0, d[i]);*
     Track[] tracksBuf = new Track[]{new Track(), new Track()};
     for (int xf = 0; xf < w; xf++) {
     for (int yf = 0; yf < h; yf++) {
     double[]d=depth.get(yf,xf);
     double x = d[0];//[pixelIndex];
     double y = d[1];//[pixelIndex];
     double z = d[2];//[pixelIndex];
     //depth test
     if ((z > MAX_TRACK_DEPTH)||(z < MIN_TRACK_DEPTH))
     continue;
     //tracks
     for (int t = 0; t < 2; t++)
     if (Math.abs(x - trackX[t]) < X_DELTA) {
     TrackPoint p = new TrackPoint();
     p.coord = new Point3(x, y, z);
     p.flatCoord = new Point(xf, yf);
     tracksBuf[t].instance.add(p);
     }
     }

     }

     long buildTime=System.currentTimeMillis();
     //System.out.println("build:"+(buildTime-time));

     //sort
     for (int t = 0; t < 2; t++) {
     ArrayList<TrackPoint> trackPoints = tracksBuf[t].instance;
     doSort(trackPoints,0,trackPoints.size()-1);
     }

     long sortTime=System.currentTimeMillis();
     //System.out.println("sort:"+(sortTime-buildTime));

     Track[] trackBuf1=new Track[]{new Track(),new Track()};
     for (int t=0; t<2; t++){
     ArrayList<TrackPoint> track=tracksBuf[t].instance;
     int i=0;
     TrackPoint p;
     for(double startDepth=MIN_TRACK_DEPTH; startDepth<MAX_TRACK_DEPTH; startDepth+=TRACK_STEP){
     double x=0,y=0,z=0;
     int xf=0,yf=0;
     int j=0;
     while ((i<track.size())&&((p=track.get(i)).coord.z-startDepth<TRACK_STEP)) {
     //TODO clustering
     x+=p.coord.x;
     y+=p.coord.y;
     z+=p.coord.z;
     xf+=p.flatCoord.x;
     yf+=p.flatCoord.y;
     i++;
     j++;
     }
     if (j!=0) {
     TrackPoint pn = new TrackPoint();
     pn.coord = new Point3(x / j, y / j, z / j);
     pn.flatCoord = new Point(xf / j, yf / j);
     trackBuf1[t].instance.add(pn);
     }
     }
     }
     return trackBuf1;
     }
     private static void doSort(ArrayList<TrackPoint> t, int start, int end) {
     if (start >= end)
     return;
     int i = start, j = end;
     int cur = i - (i - j) / 2;
     while (i < j) {
     while (i < cur && (t.get(i).coord.z <= t.get(cur).coord.z)) {
     i++;
     }
     while (j > cur && (t.get(cur).coord.z <= t.get(j).coord.z)) {
     j--;
     }
     if (i < j) {
     TrackPoint temp = t.get(i);
     t.set(i,t.get(j));
     t.set(j,temp);
     if (i == cur)
     cur = j;
     else if (j == cur)
     cur = i;
     }
     }
     doSort(t,start, cur);
     doSort(t,cur+1, end);
     }**/

}
