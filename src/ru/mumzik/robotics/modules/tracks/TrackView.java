package ru.mumzik.robotics.modules.tracks;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import ru.mumzik.opencv.util.MatUtil;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.base.View;
import ru.mumzik.robotics.modules.depth.DepthModule;
import ru.mumzik.robotics.modules.rectification.StereoRectificationModule;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class TrackView extends View {
    private static final Color LEFT_TRACK_COLOR = Color.RED;
    private static final Color RIGHT_TRACK_COLOR = Color.BLUE;
    private long[] trackId = new long[]{0};
    private long[] imgId = new long[]{0};
    private float[][] tracks;
    private BufferedImage img;
    JLabel tracksLabel = new JLabel() {
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(img, 0, 0, this);
            for (int t = 0; t < 2; t++) {
                //System.out.println("track "+t+":"+tracks[t].length/3);
                g.setColor(t == 0 ? LEFT_TRACK_COLOR : RIGHT_TRACK_COLOR);
                for (int pi = 1; pi < tracks[t].length/3; pi++) {
                    float[] xyz1=new float[3];
                    float[] xyz2=new float[3];
                    for (int ci=0; ci<3; ci++){
                        xyz1[ci]=tracks[t][(pi-1)*3+ci];
                        xyz2[ci]=tracks[t][pi*3+ci];
                    }
                    Point p1=project(xyz1);
                    Point p2=project(xyz2);
                    g.drawLine(p1.x, p1.y, p2.x, p2.y);
                }
            }
        }
    };

    private Point project(float[] point3) {
        Point point2=new Point();
        Mat QMat=StereoRectificationModule.getStereoCamera().getDispToDepthMat();
        double[] Q=new double[QMat.rows()*QMat.cols()];
        QMat.get(0,0,Q);
        point2.x=(int) (point3[0]*Q[11]/point3[2])+img.getWidth()/2;
        point2.y=(int) (point3[1]*Q[11]/point3[2])+img.getHeight()/2;
        return point2;
    }

    public TrackView() {
        super("tracks", new ModuleBase[]{TracksBuilder.getInstance(), StereoRectificationModule.getInstance(), DepthModule.getInstance()});
        content.add(tracksLabel);
        content.setLayout(new FlowLayout());
    }

    @Override
    protected void onUpdate() {
        tracks = TracksBuilder.tracks.waitForNext(trackId);
       Mat mat = StereoRectificationModule.rectifiedStereo.waitForNext(imgId)[0];
       img = MatUtil.matToImg(mat);
       /** Mat depth3 = DepthModule.depth.waitForNext(imgId);
        ArrayList<Mat> depths = new ArrayList<>();
        Core.split(depth3, depths);
        Mat depth8b = new Mat();
        Core.convertScaleAbs(depths.get(2), depth8b, 10000, 0);
        img= MatUtil.matToImg(depth8b);**/

        tracksLabel.setPreferredSize(new Dimension(img.getWidth(),img.getHeight()));
    }
}
