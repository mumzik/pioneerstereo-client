package ru.mumzik.robotics.modules.rectification;

import org.opencv.core.Mat;
import ru.mumzik.robotics.modules.DataService;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.calibration.StereoCamera;
import ru.mumzik.robotics.modules.stereo.StereoModule;
import ru.mumzik.robotics.util.ProcUtil;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class StereoRectificationModule extends ModuleBase {
    public static final DataService<Mat[]> rectifiedStereo = new DataService<>();
    private final static StereoRectificationModule instance = new StereoRectificationModule();
    private StereoCamera camera;


    private StereoRectificationModule() {
        requiredModules.add(StereoModule.getInstance());
    }

    /////////////
    //Service API
    /////////////

    public static StereoCamera getStereoCamera() {
        return instance.camera;
    }

    ///////////////////////
    //Module implementation
    ///////////////////////

    @Override
    protected File getModuleConfFile() {
        return new File(".//conf//rectification.cfg");
    }

    @Override
    protected void onStart() {
        //module dependent section
        camera = selectCameras();
        long[] stereoId =new long[]{0};
        addService(new Thread(() ->
                ProcUtil.repeat(conf.getIntVal("updateInterval"), this::isAlive, () -> {
                        Mat[] rawStereo = StereoModule.rawSetereo.waitForNext(stereoId);
                        rectifiedStereo.set(camera.undistore(rawStereo));
                })
        ));
    }

    @Override
    protected void onExit() {

    }


    public static StereoRectificationModule getInstance() {
        return instance;
    }


    /////////////////
    //private section
    /////////////////
    private StereoCamera selectCameras() {
        StereoCamera camera = null;
        JFileChooser fc = new JFileChooser(".\\cameras");
        //real code
        int ret = fc.showDialog(null, "select file for stereo camera");
        if (ret == JFileChooser.APPROVE_OPTION) {
            try {
                camera = StereoCamera.load(fc.getSelectedFile());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return camera;
    }


}
