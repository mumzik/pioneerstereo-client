package ru.mumzik.robotics.modules.debug;

import ru.mumzik.robotics.modules.base.ViewsModule;
import ru.mumzik.robotics.modules.collision.RouteBordersView;
import ru.mumzik.robotics.modules.depth.DepthView;
import ru.mumzik.robotics.modules.disparity.DisparityView;
import ru.mumzik.robotics.modules.rectification.RectifiedStereoView;
import ru.mumzik.robotics.modules.stereo.StereoView;
import ru.mumzik.robotics.modules.tracks.AltitudesView;
import ru.mumzik.robotics.modules.tracks.TrackView;

import java.awt.image.BufferedImage;
import java.io.File;

public class DebugViewModule extends ViewsModule {

    private static DebugViewModule instance = new DebugViewModule();
    private BufferedImage img;

    private DebugViewModule() {
        /**requiredModules.add(StereoRectificationModule.getInstance());
        requiredModules.add(TracksBuilder.getInstance());
        requiredModules.add(RouteBordersBuilder.getInstance());
        requiredModules.add(TrackAnalyzer.getInstance());**/
        addView(new StereoView());
        addView(new DisparityView());
        addView(new DepthView());
        addView(new RectifiedStereoView());
        addView(new TrackView());
        addView(new AltitudesView());
        addView(new RouteBordersView());
    }

    ///////////////////////
    //module implementation
    ///////////////////////
    @Override
    protected File getModuleConfFile() {
        return new File(".\\conf\\devView.cfg");
    }

    @Override
    protected void onStart() {
        /**addService(new Thread(() -> {

            JLabel dispLabel = new JLabel() {
                @Override
                public void paintComponent(Graphics g) {
                    super.paintComponents(g);
                    g.drawImage(img, 0, 0, this);
                    TracksBuilder.Track[] tracks = TracksBuilder.getTracks();
                    for (int t = 0; t < 2; t++) {
                        g.setColor(t == 0 ? Color.RED : Color.GREEN);
                        for (int i = 1; i < tracks[t].instance.size(); i++) {
                            Point p1 = tracks[t].instance.get(i - 1).flatCoord;
                            Point p2 = tracks[t].instance.get(i).flatCoord;
                            g.drawLine((int) p1.x, (int) p1.y, (int) p2.x, (int) p2.y);
                        }
                    }
                    g.setColor(TrackAnalyzer.getFreeDepth() ? Color.GREEN : Color.RED);
                    g.fillRect(0, 0, 20, 20);
                    ArrayList<RouteBordersBuilder.Section> sections = RouteBordersBuilder.getRouteBorders();
                    g.setColor(Color.BLUE);
                    for (RouteBordersBuilder.Section s : sections) {
                        for (int i = 0; i < 4; i++)
                            g.drawLine(s.corners[i].x, s.corners[i].y, s.corners[(i + 1) % 4].x, s.corners[(i + 1) % 4].y);
                    }

                }
            };
            frame.add(dispLabel);
            JPanel graphPanel = new JPanel();
            MGraph graphL = new MGraph();
            MGraph graphR = new MGraph();
            graphL.setColor(Color.RED);
            graphR.setColor(Color.GREEN);
            graphPanel.add(graphL);
            graphPanel.add(graphR);
            graphPanel.setLayout(new VerticalLayout(false));
            graphL.setGraphSize(new Dimension(300, 200));
            graphL.setYinterval(-0.4, 0);
            graphR.setGraphSize(new Dimension(300, 200));
            graphR.setYinterval(-0.4, 0);
            frame.add(graphPanel);
            frame.setLayout(new FlowLayout());
            double deltaz = 0.5;
            double deltax = 0.2;
            ProcUtil.repeat(conf.getIntVal("updateInterval"), this::isAlive, () -> {
                //Mat depth3 = DepthModule.getDepthMap();
                //  ArrayList<Mat> depths=new ArrayList<>();
                //Core.split(depth3,depths);
                //Mat depth8b=new Mat();
                //Core.convertScaleAbs(depths.get(2),depth8b,10000,0);
                TracksBuilder.Track[] tracks = TracksBuilder.getTracks();
                ArrayList<Double> x1 = new ArrayList<>();
                ArrayList<Double> x2 = new ArrayList<>();
                ArrayList<Double> y1 = new ArrayList<>();
                ArrayList<Double> y2 = new ArrayList<>();
                int n = 4;
                TracksBuilder.TrackPoint[] p = new TracksBuilder.TrackPoint[n + 1];
                for (int i = 0; i < tracks[0].instance.size() - n; i++) {
                    for (int j = 0; j < (n + 1); j++)
                        p[j] = tracks[0].instance.get(i + j);
                    boolean valid = true;
                    for (int j = 0; j < (n - 1); j++) {
                        if (Math.abs(p[j].coord.z - p[j + 1].coord.z) > deltaz)
                            valid = false;
                        if (Math.abs(p[j].coord.x - p[j + 1].coord.x) > deltax)
                            valid = false;
                    }

                    if (valid) {
                        double xsum = 0;
                        double ysum = 0;
                        for (int j = 0; j < (n + 1); j++) {
                            xsum += p[j].coord.z;
                            ysum += p[j].coord.y;
                        }
                        x1.add(xsum / n);
                        y1.add(-ysum / n);
                    }
                }
                for (int i = 0; i < tracks[1].instance.size() - n; i++) {
                    for (int j = 0; j < (n + 1); j++)
                        p[j] = tracks[1].instance.get(i + j);
                    boolean valid = true;
                    for (int j = 0; j < (n - 1); j++) {
                        if (Math.abs(p[j].coord.z - p[j + 1].coord.z) > deltaz)
                            valid = false;
                        if (Math.abs(p[j].coord.x - p[j + 1].coord.x) > deltax)
                            valid = false;
                    }

                    if (valid) {
                        double xsum = 0;
                        double ysum = 0;
                        for (int j = 0; j < (n + 1); j++) {
                            xsum += p[j].coord.z;
                            ysum += p[j].coord.y;
                        }
                        x2.add(xsum / n);
                        y2.add(-ysum / n);
                    }
                }

                double[] x1d = new double[x1.size()];
                for (int i = 0; i < x1.size(); i++)
                    x1d[i] = x1.get(i);
                double[] x2d = new double[x2.size()];
                for (int i = 0; i < x2.size(); i++)
                    x2d[i] = x2.get(i);
                double[] y1d = new double[y1.size()];
                for (int i = 0; i < y1.size(); i++)
                    y1d[i] = y1.get(i);
                double[] y2d = new double[y2.size()];
                for (int i = 0; i < y2.size(); i++)
                    y2d[i] = y2.get(i);
                graphL.setData(x1d, y1d);
                graphR.setData(x2d, y2d);


                Mat[] stereo = StereoRectificationModule.getRectifiedStereo();
                //update gui
                img = MatUtil.matToImg(stereo[0]);
                dispLabel.setIcon(new ImageIcon(img));
                // Mat left=StereoModule.getStereo()[0];
                //img = MatUtil.matToImg(left);//depth8b);
                //dispLabel.setIcon(new ImageIcon(img));
                frame.pack();
                frame.revalidate();
                frame.repaint();
            });
        }));**/
    }

    @Override
    protected void onExit() {

    }


    public static DebugViewModule getInstance() {
        return instance;
    }
}
