package ru.mumzik.robotics.modules.base;

import ru.mumzik.gui.layouts.VerticalLayout;
import ru.mumzik.robotics.exceptions.IncorrectConfigurationException;
import ru.mumzik.robotics.exceptions.ModuleAlreadyStartedException;
import ru.mumzik.robotics.util.ProcUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.DataInputStream;
import java.util.ArrayList;
import java.util.Collections;

public abstract class ViewsModule extends GuiModule {
    private static final long GUI_IPDATE_INTERVAL = 50;
    protected JMenuBar toolBox;
    protected JMenu viewsList;
    protected JPanel viewsPanel;
    private ArrayList<View> views = new ArrayList<>();

    @Override
    public void start() throws ModuleAlreadyStartedException, IncorrectConfigurationException {
        //build view's menu
        viewsList = new JMenu("views");
        for (View v : views) {
            JCheckBoxMenuItem item = new JCheckBoxMenuItem(v.getViewName());
            item.addChangeListener((e) -> {
                if (item.isSelected()) {
                    viewsPanel.add(v);
                } else {
                    viewsPanel.remove(v);
                }
            });
            viewsList.add(item);
        }

        toolBox = new JMenuBar();
        toolBox.add(viewsList);
        viewsPanel = new JPanel();
        frame.setLayout(new VerticalLayout(true));
        frame.add(toolBox);
        frame.add(viewsPanel);
        super.start();

        addService(new Thread(()->ProcUtil.repeat(GUI_IPDATE_INTERVAL, this::isAlive,()->{
            viewsPanel.revalidate();
            viewsPanel.repaint();
            frame.pack();
        })));
    }


    //for use in constructor
    protected void addView(View v) {
        views.add(v);
        Collections.addAll(requiredModules, v.getRequiredModules());
    }

}

