package ru.mumzik.robotics.modules.base;

import ru.mumzik.robotics.exceptions.IncorrectConfigurationException;
import ru.mumzik.robotics.exceptions.ModuleAlreadyStartedException;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public abstract class GuiModule extends TerminalModule {
    protected JFrame frame=new JFrame();

    @Override
    public void start() throws ModuleAlreadyStartedException, IncorrectConfigurationException {
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                exit();
            }
        });
        frame.setVisible(true);
        super.start();
    }

    @Override
    public void exit() {
        frame.dispose();
        super.exit();
    }

}
