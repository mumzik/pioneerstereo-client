package ru.mumzik.robotics.modules.base;

import ru.mumzik.robotics.exceptions.IncorrectConfigurationException;
import ru.mumzik.robotics.exceptions.ModuleAlreadyStartedException;


public abstract class TerminalModule extends ModuleBase {
    //special condition (terminals may not have services)
    private boolean terminalAlive=false;

    @Override
    public void start() throws ModuleAlreadyStartedException, IncorrectConfigurationException {
        if (isAlive()){
            throw new ModuleAlreadyStartedException();
        }
        terminalAlive=true;
        super.start();
    }

    @Override
    public void exit(){
        terminalAlive=false;
    }

    @Override
    public boolean isAlive(){
        return ((super.isAlive())||(terminalAlive));
    }

}
