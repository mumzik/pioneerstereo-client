package ru.mumzik.robotics.modules.base;

import ru.mumzik.gui.layouts.VerticalLayout;

import javax.swing.*;
import java.awt.*;

public abstract class View extends JPanel {
    public JPanel content;
    private String viewName;
    //add to required of using module
    private ModuleBase[] requiredModules;

    //override in subclasses
    public  View(String name, ModuleBase[] requiredModules) {
        this.requiredModules=requiredModules;
        this.viewName = name;
        JLabel title = new JLabel(name);
        content=new JPanel(){
            @Override
            public void paintComponent(Graphics g){
                super.paintComponent(g);
                onUpdate();
                doLayout();
            }
        };
        add(title);
        add(content);
        setLayout(new VerticalLayout(true));
    }


    protected abstract void onUpdate();



    public final String getViewName() {
        return viewName;
    }


    public final ModuleBase[] getRequiredModules() {
        return requiredModules;
    }
}
