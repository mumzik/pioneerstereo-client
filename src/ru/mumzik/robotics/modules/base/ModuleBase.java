package ru.mumzik.robotics.modules.base;

import ru.mumzik.robotics.exceptions.IncorrectConfigurationException;
import ru.mumzik.robotics.exceptions.ModuleAlreadyStartedException;
import ru.mumzik.robotics.modules.ModuleConfiguration;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public abstract class ModuleBase {
    //module dependencies
    protected final Set<ModuleBase> requiredModules = new HashSet<>();
    //module configuration
    public final ModuleConfiguration conf = new ModuleConfiguration();
    //using module counter
    private int usabilityCounter = 0;
    //threads, necessary for functioning of other modules
    private final HashSet<Thread> serviceThreads = new HashSet<>();

    /////////////////////////////
    // methods for implementation
    /////////////////////////////
    protected abstract File getModuleConfFile();

    protected abstract void onStart();

    protected abstract void onExit();

    protected void checkConfiguration() throws IncorrectConfigurationException {

    }

    ///////////////////////////////////////
    //override "public" in terminal modules
    ///////////////////////////////////////
    void start() throws ModuleAlreadyStartedException, IncorrectConfigurationException {
        conf.load(getModuleConfFile());
        checkConfiguration();
        for (ModuleBase m : requiredModules)
            m.attach();
        onStart();
    }

    void exit() {

        onExit();
        joinServices();
        for (ModuleBase m : requiredModules)
            m.detach();
    }

    /////////////////
    //service methods
    /////////////////
    public boolean isAlive() {
        return usabilityCounter > 0;
    }

    protected final void addService(Thread serviceTread) {
        serviceTread.start();
        serviceThreads.add(serviceTread);
    }

    /////////////////
    //private section
    /////////////////
    private synchronized void attach() throws IncorrectConfigurationException {
        //debug System.out.println(getClass().getName()+" attached");
        if (usabilityCounter == 0) {
            try {
                start();
            } catch (ModuleAlreadyStartedException e) {
                e.printStackTrace();
            }
        }
        usabilityCounter++;
    }


    private synchronized void detach() {
        //debug System.out.println(getClass().getName()+" detached");
        usabilityCounter--;
        if (usabilityCounter == 0)
            exit();
    }


    private void joinServices() {
        for (Thread t : serviceThreads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
