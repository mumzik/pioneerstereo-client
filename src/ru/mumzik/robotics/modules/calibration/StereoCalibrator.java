package ru.mumzik.robotics.modules.calibration;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import ru.mumzik.opencv.util.MatUtil;

import java.util.ArrayList;
import java.util.List;

public class StereoCalibrator {
    private final List<Mat>[] cornersList = new List[]{new ArrayList<Mat>(),new ArrayList<Mat>()};
    private final List<Mat> patternPointsList = new ArrayList<>();
    private final Mat camMatrix[] = new Mat[]{new Mat(), new Mat()};
    private final Mat distCoeffs[] = new Mat[]{new Mat(), new Mat()};
    private int w, h;

    public StereoCalibrator(int width, int height) {
        w = width;
        h = height;
    }

    public synchronized void addPattern(Mat[] stereo, int boardWidth, int boardHeight) throws Exception {
        boolean found = true;
        //create pattern points
        int numSquares = boardHeight * boardWidth;
        Mat patternPoints = new Mat();
        for (int j = 0; j < numSquares; j++)
            patternPoints.push_back(new MatOfPoint3f(new Point3(j / boardWidth, j % boardWidth, 0.0f)));

        Mat[] cornersBuf=new Mat[2];
        for (int i = 0; i < 2; i++) {
            //grey
            Mat grayImage = MatUtil.to8bit(stereo[i]);
            //find corners
            Size boardSize = new Size(boardWidth, boardHeight);
            MatOfPoint2f corners = new MatOfPoint2f();
            found = found && Calib3d.findChessboardCorners(grayImage, boardSize, corners,
                    Calib3d.CALIB_CB_NORMALIZE_IMAGE);
            cornersBuf[i]=corners;
        }
            if (found) {
                cornersList[0].add(cornersBuf[0]);
                cornersList[1].add(cornersBuf[1]);
                patternPointsList.add(patternPoints);
            } else
                throw new Exception("Pattern not found");
    }

    public synchronized StereoCamera calibrate() {
        /**for (int i=0; i<2; i++){
            List<Mat> rvecs = new ArrayList<>();//ignored parameters
            List<Mat> tvecs = new ArrayList<>();//
            Calib3d.calibrateCamera(patternPointsList, cornersList[i], new Size(w, h), camMatrix[i], distCoeffs[i], rvecs, tvecs);
        }**/
        Mat E=new Mat();
        Mat F=new Mat();
        Mat R=new Mat();
        Mat T=new Mat();
        List<Mat> rvecs = new ArrayList<>();//ignored parameters
        List<Mat> tvecs = new ArrayList<>();//
        Calib3d.calibrateCamera(patternPointsList, cornersList[0], new Size(w, h), camMatrix[0], distCoeffs[0], rvecs, tvecs);
        Calib3d.calibrateCamera(patternPointsList, cornersList[1], new Size(w, h), camMatrix[1], distCoeffs[1], rvecs, tvecs);
        Calib3d.stereoCalibrate(
                patternPointsList,
                cornersList[0],
                cornersList[1],
                camMatrix[0],
                distCoeffs[0],
                camMatrix[1],
                distCoeffs[1],
                new Size(w,h),
                R,
                T,
                E,
                F
        );
        //DEBUG
        System.out.println("cam matrix 1");
        MatUtil.printMat(camMatrix[0],2);
        System.out.println("cam matrix 2");
        MatUtil.printMat(camMatrix[1],2);
        System.out.println("dist coeffs 1");
        MatUtil.printMat(distCoeffs[0],2);
        System.out.println("dist coeffs 2");
        MatUtil.printMat(distCoeffs[1],2);
        return new StereoCamera(camMatrix,distCoeffs,R,T,new int[]{w,h});
    }


}
