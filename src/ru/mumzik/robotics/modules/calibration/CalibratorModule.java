package ru.mumzik.robotics.modules.calibration;

import org.opencv.core.Mat;
import ru.mumzik.robotics.Terminal;
import ru.mumzik.robotics.modules.base.ViewsModule;
import ru.mumzik.robotics.modules.base.TerminalModule;
import ru.mumzik.robotics.modules.stereo.StereoModule;
import ru.mumzik.robotics.modules.stereo.StereoView;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class CalibratorModule extends ViewsModule {
    private StereoCalibrator calibrator;
    private static CalibratorModule instance = new CalibratorModule();

    private CalibratorModule() {
        requiredModules.add(StereoModule.getInstance());
        addView(new StereoView());
    }


    ///////////////////////
    //module implementation
    ///////////////////////
    @Override
    protected File getModuleConfFile() {
        return new File(".\\conf\\calibration.cfg");
    }

    @Override
    protected void onStart() {
        JButton patternBut = new JButton("add current pattern");
        JButton calibrateBut = new JButton("calibrate");
        toolBox.add(patternBut);
        toolBox.add(calibrateBut);

        JLabel widthL = new JLabel("board width");
        JTextField widthEdit = new JTextField("8");
        JLabel heightL = new JLabel("board height");
        JTextField heightEdit = new JTextField("6");

        toolBox.add(widthL);
        toolBox.add(widthEdit);
        toolBox.add(heightL);
        toolBox.add(heightEdit);


        //add pattern button's push action
        patternBut.addActionListener((event) -> {
            try {
                //chess table size
                int w = Integer.parseInt(widthEdit.getText());
                int h = Integer.parseInt(heightEdit.getText());

                Mat[] stereo=StereoModule.rawSetereo.get();
                //for first pattern in group
                if (calibrator == null)
                    calibrator = new StereoCalibrator(stereo[0].width(), stereo[0].height());
                //add patterns
                calibrator.addPattern(stereo, w, h);
            } catch (NumberFormatException e) {
                Terminal.error("wrong number format"); //TODO
            } catch (Exception e) {
                Terminal.error("can not find a pattern");
            }
        });


        //calibrate button's push action
        calibrateBut.addActionListener((event) -> {
            try {
                //save left camera
                JFileChooser fc = new JFileChooser();
                int ret = fc.showDialog(null, "select file for stereo camera");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    calibrator.calibrate().save(fc.getSelectedFile());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    protected void onExit() {

    }

    public static TerminalModule getInstance() {
        return instance;
    }
}
