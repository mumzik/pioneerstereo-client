package ru.mumzik.robotics.modules.calibration;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import ru.mumzik.opencv.util.MatUtil;

import java.io.*;

public class StereoCamera {
    private Mat camMatrix[]=new Mat[]{new Mat(), new Mat()};
    private Mat distCoeffs[]=new Mat[]{new Mat(), new Mat()};
    private Mat Q = new Mat();
    private int[] size=new int[2] ;

    StereoCamera(Mat[] camMatrix, Mat[] distCoeffs, Mat R, Mat T, int[] size) {
        this.camMatrix = camMatrix;
        this.distCoeffs = distCoeffs;
        this.size = size;
        Mat R1 = new Mat();
        Mat R2 = new Mat();
        Mat P1 = new Mat();
        Mat P2 = new Mat();
        Calib3d.stereoRectify(camMatrix[0],distCoeffs[0],camMatrix[1],distCoeffs[1],new Size(size[0],size[1]),R,T,R1,R2,P1,P2,Q);
    }

    private StereoCamera() {
    }

    public void save(File dstFile) throws IOException {
        DataOutputStream out = new DataOutputStream(new FileOutputStream(dstFile));
        out.writeInt(size[0]);
        out.writeInt(size[1]);
        for (int i = 0; i < 2; i++) {
            MatUtil.serializeMat(out, camMatrix[i]);
            MatUtil.serializeMat(out, distCoeffs[i]);
        }
        MatUtil.serializeMat(out, Q);
        out.flush();
        out.close();
    }

    public Mat getDispToDepthMat(){
        return Q;
    }

    public static StereoCamera load(File srcFile) throws IOException {
        StereoCamera cam = new StereoCamera();
        DataInputStream in = new DataInputStream(new FileInputStream(srcFile));
        cam.size[0] = in.readInt();
        cam.size[1] = in.readInt();
        for (int i = 0; i < 2; i++) {
            cam.camMatrix[i] = MatUtil.deserializeMat(in);
            cam.distCoeffs[i] = MatUtil.deserializeMat(in);
        }
        cam.Q = MatUtil.deserializeMat(in);
        in.close();
        return cam;
    }

    public Mat[] undistore(Mat[] src) {
        Mat[] undistored = new Mat[]{new Mat(), new Mat()};
        Imgproc.undistort(src[0], undistored[0], camMatrix[0], distCoeffs[0]);
        Imgproc.undistort(src[1], undistored[1], camMatrix[1], distCoeffs[1]);
        return undistored;
    }
}
