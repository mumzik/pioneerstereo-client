package ru.mumzik.robotics.util;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class ImgBuilder {
    /**
     * bytesToImg buffered image from byte array
     */
    public static  BufferedImage bytesToImg(byte[] data) throws IOException {
        if (data==null)
            throw new IllegalArgumentException("data is empty");
        return ImageIO.read(new ByteArrayInputStream(data));
    }

    /**
     * bytesToImg array of two buffered images from byte array
     */
    public static BufferedImage[] buildStereo(byte[] data) throws IOException {
        //validate data
        if ((data==null)||(data.length<8))
            throw new IllegalArgumentException("data is empty");
        //calibrate stream
        DataInputStream r = new DataInputStream(new ByteArrayInputStream(data));
        //get size
        int leftSize = r.readInt();
        int rightSize = r.readInt();
        if ((leftSize <= 0) || (rightSize <= 0))
            throw new IllegalArgumentException("data is empty");
        //bytesToImg stereo
        BufferedImage[] stereo = new BufferedImage[2];
        byte[] leftB = new byte[leftSize];
        r.read(leftB,0,leftSize);
        stereo[0] = bytesToImg(leftB);
        byte[] rightB = new byte[rightSize];
        r.read(rightB,0,rightSize);
        stereo[1] = bytesToImg(rightB);
        return stereo;
    }

    public static byte[] ImgToBytes(BufferedImage image) throws IOException {
        ByteOutputStream stream=new ByteOutputStream();
        ImageIO.write(image,"bmp",stream);
        return stream.getBytes();
    }



}
