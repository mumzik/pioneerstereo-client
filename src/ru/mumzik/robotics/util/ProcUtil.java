package ru.mumzik.robotics.util;

public class ProcUtil {

    public static void repeat(long interval, Condition condition , Runnable iteration) {
        long t, dt;
        while (condition.check()) {
            t = System.currentTimeMillis();
            iteration.run();
            if ((dt = System.currentTimeMillis() - t) < interval)
                try {
                    Thread.sleep(interval - dt);
                } catch (InterruptedException ignored) {
                }
        }
    }

}
