package ru.mumzik.robotics.util;

public interface Condition {
    boolean check();
}
