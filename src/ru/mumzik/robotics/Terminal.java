package ru.mumzik.robotics;

import javafx.util.Pair;
import org.opencv.core.Core;
import ru.mumzik.robotics.exceptions.IncorrectConfigurationException;
import ru.mumzik.robotics.exceptions.ModuleAlreadyStartedException;
import ru.mumzik.robotics.exceptions.UnknownCommandException;
import ru.mumzik.robotics.modules.debug.DebugViewModule;
import ru.mumzik.robotics.modules.base.ModuleBase;
import ru.mumzik.robotics.modules.base.TerminalModule;
import ru.mumzik.robotics.modules.calibration.CalibratorModule;
import ru.mumzik.robotics.modules.movements.MovementModule;
import ru.mumzik.util.MSimpleObservable;

import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Terminal {
    //Command regexp patterns
    private static final Pattern CMD_START_MODULE = Pattern.compile("^\\s*start\\s+(?<moduleName>\\w+)\\s*$");
    private static final Pattern CMD_STOP_MODULE = Pattern.compile("^\\s*stop\\s+(?<moduleName>\\w+)\\s*$");
    private static final Pattern CMD_HELP = Pattern.compile("^\\s*help\\s*$");
    private static final Pattern CMD_LIST = Pattern.compile("^\\s*mlist\\s*$");
    private static final Pattern CMD_QUIT = Pattern.compile("^\\s*quit|q\\s*$");
    //modules' names
    private static final HashMap<String, TerminalModule> moduleNameMap = new HashMap<String, TerminalModule>() {{
        put("calib", CalibratorModule.getInstance());
        put("movepad", MovementModule.getInstance());
        put("v", DebugViewModule.getInstance());//TODO
    }};

    private static boolean isAlive = false;
    private static MSimpleObservable onExit = new MSimpleObservable();


    public static void main(String[] args) {
        start();
    }


    private static void start() {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        System.out.println("Pioneer control system");
        Scanner scanner = new Scanner(System.in);
        isAlive = true;
        while (isAlive) {
            System.out.print(">");
            String s = scanner.nextLine();
            try {
                execCommand(s);
            } catch (UnknownCommandException e) {
                error("unknown command:" + s + ". write \"help\" for print the list of available commands");
            }
        }
    }


    private static void execCommand(String cmd) throws UnknownCommandException {
        Matcher m;
        m = CMD_START_MODULE.matcher(cmd);
        if (m.matches()) {
            startModule(m.group("moduleName"));
            return;
        }
        m = CMD_STOP_MODULE.matcher(cmd);
        if (m.matches()) {
            stopModule(m.group("moduleName"));
            return;
        }
        m = CMD_HELP.matcher(cmd);
        if (m.matches()) {
            printHelp();
            return;
        }
        m = CMD_LIST.matcher(cmd);
        if (m.matches()) {
            printModuleList();
            return;
        }
        m = CMD_QUIT.matcher(cmd);
        if (m.matches()) {
            exit();
            return;
        }
        throw new UnknownCommandException();
    }

    //////////
    //commands
    //////////
    private static void printModuleList() {
        for (String s : moduleNameMap.keySet())
            System.out.println("\t" + s);
    }

    private static void printHelp() {
        //TODO
        System.out.println("\tstart <moduleName> - start specified terminal module");
        System.out.println("\tstop <moduleName>  - forced off specified module");
        System.out.println("\tmlist              - display module list");
        System.out.println("\thelp               - print help");
        System.out.println("\tquit|q             - quit from Pioneer control system");
    }


    private static void exit() {
        isAlive = false;
        onExit.fireEvent();
    }


    private static void startModule(String moduleName) {
        if (moduleNameMap.containsKey(moduleName)) {
            TerminalModule module = moduleNameMap.get(moduleName);
            try {
                module.start();
            } catch (ModuleAlreadyStartedException e) {
                e.printStackTrace();
                error("module  \"" + moduleName + "\" has already started");
            } catch (IncorrectConfigurationException e) {
                Pair<String, String> parameter = e.getIncorrectParameter();
                error("incorrect configuration(" + e.getThrowsModule().getClass().getSimpleName() + "):\n" +
                        "\tkey - \"" + parameter.getKey() + "\"\n" +
                        "\tvalue - \"" + parameter.getValue() + "\"");
            }
            onExit.addListener(module::exit);
        } else
            error("module with name \"" + moduleName + "\" not exists");
    }


    private static void stopModule(String moduleName) {
        if (moduleNameMap.containsKey(moduleName)) {
            TerminalModule module = moduleNameMap.get(moduleName);
            if (!module.isAlive())
                error("module \"" + moduleName + "\" has not started");
            else
                module.exit();
        } else
            error("module with name \"" + moduleName + "\" not exists");
    }

    //////////////
    //errors print
    //////////////
    public static void criticalError(String message) {
        System.out.println(">CRITICAL ERROR!!! " + message);
        System.exit(1);
    }


    public static void error(String message) {
        System.out.println(">ERR " + message);
    }

    public static void error(ModuleBase module, String message) {
        error("(" + module.getClass().getSimpleName() + "): " + message);
    }

}
