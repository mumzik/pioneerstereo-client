﻿  
float absVal(float val){
	if (val<0)
		return -val;
	else
		return val;
}
  
__kernel void tracksKernel (
		__global const float * depth,
		__global const float * parameters,
		__global const int* res,
		__global const int* tileCnt,
		__global float * t1,
		__global float * t2,
		__global int* size
		)
	{
		//service variables
		int tile = get_global_id(0);
		int width=res[0]; 
		int height=res[1];
		int depthLen=width*height; 
		int startIndex=(depthLen*3)*tile/tileCnt[0];
		int endIndex=(depthLen*3)*(tile+1)/tileCnt[0];
		
		float nearBorder=parameters[0];
		float farBorder=parameters[1];
		float leftX=parameters[2];
		float rightX=parameters[3];
		float delta=parameters[4];
		
		float x;
		float y;
		float z;
		int index;
		for (int i=startIndex; i<endIndex; i+=3){
			z=depth[i+2];
			//Z TEST

			if ((z > nearBorder) && (z < farBorder)){
				x = depth[i+0];

				//LEFT TRACK
				if (absVal(x-leftX)<delta){
					index = atomic_add(&(size[0]),1)*3;
					y=depth[i+1];
					t1[index]=x;
					t1[index+1]=y;
					t1[index+2]=z;
				}else
				//RIGHT TRACK
				if (absVal(x-rightX)<delta){
					index = atomic_add(&(size[1]),1)*3;
					y=depth[i+1];
					t2[index]=x;
					t2[index+1]=y;
					t2[index+2]=z;
				}
			}
		}
	}  
